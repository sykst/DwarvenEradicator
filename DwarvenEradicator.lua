DEFAULT_CHAT_FRAME:AddMessage("Dwarven Eradicator primed!");

DwarvenEradicator = CreateFrame("Frame", nil)
DwarvenEradicator:RegisterEvent("PLAYER_AURAS_CHANGED");

DwarvenEradicator:SetScript("OnEvent", function ()
	if (event == "PLAYER_AURAS_CHANGED") then
		local i = 0;
		while not (GetPlayerBuff(i, "HELPFUL") == -1) do
			local buffIndex, untilCancelled = GetPlayerBuff(i, "HELPFUL")
			local texture = GetPlayerBuffTexture(buffIndex);

			if (string.find(texture,"Incinerate")) then
				CancelPlayerBuff(buffIndex);
				DEFAULT_CHAT_FRAME:AddMessage("Fury of Forgewright Eradicated!");
			end

			i = i + 1;
		end
	end
end)
