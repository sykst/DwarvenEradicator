# Dwarven Eradicator

A very simple addon which automatically removes Fury of the Forgewright, which is a buff applied by Ironfoe, which forces you into speaking Dwarvish.

## Fury of the Forgewright eradicated!

```Fury of the Forgewright eradicated!```   
will be printed each time FotF is removed.  

If you want to disable it, open `DwarvenEradicator.lua` file and remove the line:  

```DEFAULT_CHAT_FRAME:AddMessage("Fury of Forgewright Eradicated!");```

Cheers!
